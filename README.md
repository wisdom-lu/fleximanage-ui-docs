# fleximanage-ui

#### 介绍
基于Flexiwan Manager 构建的Flexiwan Manager UI,目前支持Flexiwan manage 3.3.5版本

#### 软件架构
采用VUE-Element-admin技术框架实现与Flexiwan后台对接完成所有管理功能


#### 安装教程

1.  yarn install
2.  yarn build:prod

#### 使用说明

1.  配置flexiwan后台地址（.env.development）（http://manage.flexiwan.com/api）
2.  yarn dev

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### UI演示效果
1.  Network 拓扑
    ![网络拓扑](https://images.gitee.com/uploads/images/2021/0508/161050_2c732689_8701876.png "12.png")
2.  Device设备列表
    ![设备矩阵](https://images.gitee.com/uploads/images/2021/0508/161110_451490b2_8701876.png "13.png")
    ![设备详细](https://images.gitee.com/uploads/images/2021/0508/162831_0b34cc28_8701876.png "123.png")

#### 特技

1.  此项目个人开发，如需使用请联系微信![扫码加微信](https://images.gitee.com/uploads/images/2021/0508/160920_c034167e_8701876.jpeg "1.jpg")
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
